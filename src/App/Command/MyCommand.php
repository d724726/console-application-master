<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use Symfony\Component\Serializer\Serializer; 
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;


/**
 * Class MyCommand
 *
 * @package App\Command
 */
class MyCommand extends Command
{
    use XmlRead;
    use CSVExport;
    use ErrorLogs;

    protected $filePath;
    /**
     * @return void
     */
    protected function configure(): void
    {
        $this -> setName('importXML')
            -> setDescription('XML path.')
            -> setHelp('Enter XML path.')
            -> addArgument('importXML', InputArgument::REQUIRED, 'XML PATH REQUIRED..');
        
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        try{
            // Set Import XML Path
            $this->filePath = $input -> getArgument('importXML');

            $validate = $this->validateXM($this->filePath);
            if($validate['status'] == '1'){

                $output->writeln('Error: '.$validate['message']);
                $this->errorLog('Error: '.$validate['message']);
                exit();
            }


            $output->writeln('Start Import XML file....');

            // Read XML file
            $xmlData = $this->getXMLDataAsArray($this->filePath);

            $output->writeln('XML file read successfully....');

            if ($xmlData['status'] === '0') {

                $output->writeln('Start creating CSV file.');

                $csvFileName = $this->exportCSV($xmlData['body'],$xmlData['header']);

                $output->writeln($csvFileName .' CSV file created successfully!');
            }

            if($xmlData['status'] === '1'){
                $output->writeln('Error: '.$xmlData['message']);
                $this->errorLog('Error: '.$xmlData['message']);
                
            }
            $output->writeln('EXIT....');

        } catch (Exception $e) {
            $this->errorLog($e->getMessage());
        }
        exit;
        //return Command::SUCCESS;
    }

}


/**
 * trait ErrorLogs
 *
 */
trait ErrorLogs{

    public function errorLog($error_message='')
    {
        // path of the log file where errors need to be logged
        $log_file = "./log/".date('Y-m-d')."-errors.log";
          
        // logging error message to given log file
        error_log($error_message, 3, $log_file);
    }
}


/**
 * trait CSVExport
 *
 */
trait CSVExport {

    public function exportCSV($data,$header=array())
    {
        $csvFileName = "public/sample_".time().".csv";
        
        $csvfile = fopen($csvFileName, "w");

        if (!empty($header)) {
            fputcsv($csvfile, $header);
        }
        
        
        foreach ($data as $line)
        {
            fputcsv(
                $csvfile, // The file pointer
                $line, // The fields
                ',' // The delimiter
            );      
        }     
        
        fclose($csvfile);
        return $csvFileName; 
    }
}


/**
 * trait XmlRead
 *
 */
trait XmlRead 
{
    public function validateXM($filename)
    {
        if (file_exists($filename)) {

            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, array('xml'))) {
                return array('status'=>'1', 'message' =>'Error to load XML File.');
            }else{
                return array('status'=>'0', 'message' =>'Valid XML file.');
            }
            
        } else {
            return array('status'=>'1', 'message' =>'XML File not found.');
        }
    }
    // GET XML DATA
    public function getXMLDataAsArray($filePath)
    {
        $decoder = new Serializer([new ObjectNormalizer()],[new XmlEncoder()]);

        $xmlData = $decoder->decode(file_get_contents($filePath),'xml');

        if ( isset($xmlData['row'])  && count($xmlData['row']) > 0) {

            $header =  array_keys($xmlData['row'][0]);

            $xmlData = $this->FilterXMLData($xmlData['row']);

            return array('status'=>'0', 'header' => $header,'body' => $xmlData);

        }else{
            return array('status'=>'1', 'message' =>'No data found in XML file.');
        }
        
    }

    // Filter XML Data
    public function FilterXMLData($xmlData)
    {
        $finaData = [];
        foreach ($xmlData as $file) {
            $result = [];
            array_walk_recursive($file, function($item) use (&$result) {
                $result[] = $item;
            });
            $finaData[] = $result;
        }
        return $finaData;
    }

}
